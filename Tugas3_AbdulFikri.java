package Tugas3;

import java.util.ArrayList;
import java.util.Scanner;
//Abdul Fikri Zaki 
//235150400111007
//SI A

class Mahasiswa {
    
    private String NIM_MHS;
    private String Nm_MHS;
    private ArrayList<MataKuliah> MatKuliah = new ArrayList<>();
    
    public ArrayList<MataKuliah> getMatkul() {
        return MatKuliah;
    }

    public void setMatkul(ArrayList<MataKuliah> mataKuliah) {
        this.MatKuliah = mataKuliah;
    }

    public String getNIM_MHS() {
        return NIM_MHS;
    }

    public void setNIM_MHS(String nimMahasiswa) {
        this.NIM_MHS = nimMahasiswa;
    }

    public String getNm_MHS() {
        return Nm_MHS;
    }

    public void setNm_MHS(String namaMahasiswa) {
        this.Nm_MHS = namaMahasiswa;
    }
}

class MataKuliah {

    private float valueAngka;
    private char valueHuruf;
    private String Kode_Matkul;
    private String Nm_Matkul;

    public String getKode_Matkul() {
        return Kode_Matkul;
    }

    public String getNm_Matkul() {
        return Nm_Matkul;
    }

    public void setNm_Matkul(String namaMataKul) {
        this.Nm_Matkul = namaMataKul;
    }

    public void setValueAngka(float valueAngka) {
        this.valueAngka = valueAngka;
    }

    public void setKode_Matkul(String kodeMatkul) {
        this.Kode_Matkul = kodeMatkul;
        
    }
    
    public char getValueHuruf() {
        int nilai = (int)valueAngka;
        if (nilai >= 80) {
            valueHuruf = 'A';
        }
        else if (nilai >= 60) {
            valueHuruf = 'B';
        }
        else if (nilai >= 50) {
            valueHuruf = 'C';
        }
        else if (nilai >= 40) {
            valueHuruf = 'D';
        }
        else {
            valueHuruf = 'E';
        }
        return valueHuruf;
    }

}

public class Main {
    public static void main(String[] args) {
        ArrayList<Mahasiswa> MHSSW = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        boolean TambahMahasiswa = true;
        while (TambahMahasiswa) {
  
            System.out.print("Masukkan nama :");
            String Nm_MHS = scanner.nextLine();

            System.out.print("Masukkan NIM :");
            String NIM_Mhs = scanner.nextLine();

            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setNm_MHS(Nm_MHS);
            mahasiswa.setNIM_MHS(NIM_Mhs);

            ArrayList<MataKuliah> Matkul = new ArrayList<>();
            boolean nextMataKuliah = true;
            while (nextMataKuliah) {
                System.out.print("Masukkan Nama Matkul:");
                String Nama_MataKuliah = scanner.nextLine();

                System.out.print("Masukkan Kode Matkul:");
                String Kode_MataKuliah = scanner.nextLine();

                System.out.print("Masukkan Nilai Matkul:");
                float Nilai_MataKuliah = scanner.nextFloat();

                MataKuliah mataKuliah = new MataKuliah();
                mataKuliah.setKode_Matkul(Kode_MataKuliah);
                mataKuliah.setNm_Matkul(Nama_MataKuliah);
                mataKuliah.setValueAngka(Nilai_MataKuliah);
                Matkul.add(mataKuliah);
                scanner.nextLine();
                System.out.print("Apakah ingin menambah matkul lain? (Ketik \"y\" jika Ya, ketik \"x\" jika Tidak):");
                String TambahMatkul = scanner.nextLine();

                if (TambahMatkul.equals("x")) {
                    nextMataKuliah = false;
                }
            }

            mahasiswa.setMatkul(Matkul);
            MHSSW.add(mahasiswa);
            System.out.print("Apakah ingin menambah mahasiswa lain? (Ketik \"y\" jika Ya, ketik \"x\" jika Tidak): ");
            String tambahMahasiswa = scanner.nextLine();

            if (tambahMahasiswa.equals("x")) {
                TambahMahasiswa = false;
            }
        }

        for (int i = 0; i < MHSSW.size(); i++) {
            System.out.println("========================================================================");
            Mahasiswa mahasiswa = (Mahasiswa) MHSSW.get(i);
            System.out.println("Nama: " + mahasiswa.getNm_MHS());
            System.out.println("NIM: " + mahasiswa.getNIM_MHS());
            System.out.println("------------------------------------------------------------------------");
    
            ArrayList<MataKuliah> mataKuliahs = mahasiswa.getMatkul();
            for (int j = 0; j < mataKuliahs.size(); j++ ) {
                MataKuliah matKuliah = (MataKuliah) mataKuliahs.get(j);
                System.out.println("Kode Matkul: " + matKuliah.getKode_Matkul() + " | Nama Matkul: " + matKuliah.getNm_Matkul() + " | Nilai: " + matKuliah.getValueHuruf());
            }
            System.out.println("========================================================================");            
            System.out.println();
            System.out.println();
        }
        scanner.close();
    }
    
}


